<?php

namespace PSNDL\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Package
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="PSNDL\MainBundle\Entity\PackageRepository")
 *
 * @ORM\HasLifecycleCallbacks()
 */
class Package
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="PackageID", type="string", length=255)
     * @Assert\NotBlank(message="This field is required.")
     * @Assert\Regex(pattern="/[A-Za-z]{4}[0-9]{5}/", message="Invalid package ID.")
     */
    private $packageID;

    /**
     * @var string
     *
     * @ORM\Column(name="Title", type="string", length=255)
     * @Assert\NotBlank(message="This field is required.")
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="DownloadURL", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Url(message="Invalid download url.")
     */
    private $downloadURL;

    /**
     * @var string
     *
     * @ORM\Column(name="RapName", type="string", length=40, nullable=true)
     * @Assert\Regex(pattern="/[A-Z]{2}[0-9]{4}-[A-Z]{4}[0-9]{5}_[0-9]{2}-[A-Z0-9_-]{16}.(RAP|rap)/", message="Invalid rap name.")
     */
    private $rapName;

    /**
     * @var string
     *
     * @ORM\Column(name="RapData", type="string", length=32, nullable=true)
     * @Assert\Regex(pattern="/[A-F0-9]{32}/", message="The data must be 32 bytes hex string.")
     */
    private $rapData;

    /**
     * @var string
     *
     * @ORM\Column(name="Description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="Author", type="string", length=255)
     * @Assert\NotBlank(message="This field is required.")
     */
    private $author;

    /**
     * @var boolean
     *
     * @ORM\Column(name="Approved", type="boolean")
     */
    private $approved;

    /**
     * @var integer
     *
     * @ORM\Column(name="DownloadCount", type="integer")
     */
    private $downloadCount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="AddedDate", type="date")
     */
    private $addedDate;

    /**
     * @var \PSNDL\MainBundle\Entity\Category
     *
     * @ORM\ManyToOne(targetEntity="PSNDL\MainBundle\Entity\Category", inversedBy="packages")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="This field is required.")
     */
    private $category;

    /**
     * @var \PSNDL\MainBundle\Entity\Region
     *
     * @ORM\ManyToOne(targetEntity="PSNDL\MainBundle\Entity\Region", inversedBy="packages")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="This field is required.")
     */
    private $region;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set packageID
     *
     * @param string $packageID
     *
     * @return Package
     */
    public function setPackageID($packageID)
    {
        $this->packageID = $packageID;

        return $this;
    }

    /**
     * Get packageID
     *
     * @return string
     */
    public function getPackageID()
    {
        return $this->packageID;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Package
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set downloadURL
     *
     * @param string $downloadURL
     *
     * @return Package
     */
    public function setDownloadURL($downloadURL)
    {
        $this->downloadURL = $downloadURL;

        return $this;
    }

    /**
     * Get downloadURL
     *
     * @return string
     */
    public function getDownloadURL()
    {
        return $this->downloadURL;
    }

    /**
     * Set rapName
     *
     * @param string $rapName
     *
     * @return Package
     */
    public function setRapName($rapName)
    {
        $this->rapName = $rapName;

        return $this;
    }

    /**
     * Get rapName
     *
     * @return string
     */
    public function getRapName()
    {
        return $this->rapName;
    }

    /**
     * Set rapData
     *
     * @param string $rapData
     *
     * @return Package
     */
    public function setRapData($rapData)
    {
        $this->rapData = $rapData;

        return $this;
    }

    /**
     * Get rapData
     *
     * @return string
     */
    public function getRapData()
    {
        return $this->rapData;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Package
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set author
     *
     * @param string $author
     *
     * @return Package
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set approved
     *
     * @param boolean $approved
     *
     * @return Package
     */
    public function setApproved($approved)
    {
        $this->approved = $approved;

        return $this;
    }

    /**
     * Get approved
     *
     * @return boolean
     */
    public function getApproved()
    {
        return $this->approved;
    }

    /**
     * Set downloadCount
     *
     * @param integer $downloadCount
     *
     * @return Package
     */
    public function setDownloadCount($downloadCount)
    {
        $this->downloadCount = $downloadCount;

        return $this;
    }

    /**
     * Get downloadCount
     *
     * @return integer
     */
    public function getDownloadCount()
    {
        return $this->downloadCount;
    }

    /**
     * Set addedDate
     *
     * @param \DateTime $addedDate
     *
     * @return Package
     */
    public function setAddedDate($addedDate)
    {
        $this->addedDate = $addedDate;

        return $this;
    }

    /**
     * Get addedDate
     *
     * @return \DateTime
     */
    public function getAddedDate()
    {
        return $this->addedDate;
    }

    /**
     * Set category
     *
     * @param \PSNDL\MainBundle\Entity\Category $category
     *
     * @return Package
     */
    public function setCategory(\PSNDL\MainBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \PSNDL\MainBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set region
     *
     * @param \PSNDL\MainBundle\Entity\Region $region
     *
     * @return Package
     */
    public function setRegion(\PSNDL\MainBundle\Entity\Region $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \PSNDL\MainBundle\Entity\Region
     */
    public function getRegion()
    {
        return $this->region;
    }



    /** @ORM\PrePersist */
    public function onPackagePrepersist()
    {
        $this->setDownloadCount(0);
        $this->setApproved(false);
        $this->setAddedDate(new \DateTime());
    }

}
