<?php

namespace PSNDL\MainBundle\Controller;

use Exception;
use PSNDL\MainBundle\Entity\Package;
use PSNDL\MainBundle\Form\PackageEditType;
use PSNDL\MainBundle\Form\PackageType;
use PSNDL\MainBundle\PackageInfo\PackageInfo;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PackagesController extends Controller
{
    public function packagesAction($page, Request $request)
    {
        if ($page < 1) {
            throw $this->createNotFoundException('The page ' . $page . " doesn't exist.");
        }

        $orderBy = 'name';
        $order = 'asc';

        if (isset($_GET['orderBy'])) $orderBy = $_GET['orderBy'];
        if (isset($_GET['order'])) $order = $_GET['order'];

        $searchStr = $request->query->get('txt_search');

        $packageRepository = $this->getDoctrine()->getManager()->getRepository('PSNDLMainBundle:Package');

        $nbPerPage = $this->getParameter('packages_per_page');
        $packages = $packageRepository->getPackages(true, $page, $nbPerPage, $orderBy, $order, $searchStr);

        $pagesCount = ceil(count($packages) / $nbPerPage);
        if ($pagesCount == 0) $pagesCount++;


        if ($page > $pagesCount) {
            throw $this->createNotFoundException('The page ' . $page . " doesn't exist.");
        }

        return $this->render('PSNDLMainBundle:Packages:packages.html.twig', array(
            'packages' => $packages,
            'pagesCount' => $pagesCount,
            'page' => $page,
            'txt_search' => $searchStr
        ));
    }

    public function packagesUnderApprovalAction($page)
    {
        $packageRepository = $this->getDoctrine()->getManager()->getRepository('PSNDLMainBundle:Package');

        $nbPerPage = $this->getParameter('packages_per_page');
        $packages = $packageRepository->getPackages(false, $page, $nbPerPage);

        $pagesCount = ceil(count($packages) / $nbPerPage);
        if ($pagesCount == 0) $pagesCount++;


        if ($page > $pagesCount) {
            throw $this->createNotFoundException('The page ' . $page . " doesn't exist.");
        }

        return $this->render('PSNDLMainBundle:Packages:packages_approval.html.twig', array(
            'packages' => $packages,
            'pagesCount' => $pagesCount,
            'page' => $page
        ));
    }

    public function viewPackageAction($id)
    {
        $package = $this->getDoctrine()->getRepository('PSNDLMainBundle:Package')->find($id);

        if ($package === null) {
            $this->addFlash('danger', "Couldn't find the requested package.");
            return $this->redirectToRoute('psndl_main_packages');
        }

        $canEdit = $this->isGranted('ROLE_ADMIN');

        return $this->render('@PSNDLMain/Packages/view_package.html.twig', array('package' => $package, 'canEdit' => $canEdit));
    }

    /**
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     */
    public function addPackageAction(Request $request)
    {
        $package = new Package();
        $form = $this->createForm(PackageType::class, $package, array('author' => $this->getUser()->getUsername()));

        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()) {
            $em = $this->getDoctrine()->getEntityManager();

            $em->persist($package);
            $em->flush();

            $this->addFlash('success', 'Successfully submitted the package.');

            return $this->redirectToRoute('psndl_main_packages_under_approval');
        }
        return $this->render('PSNDLMainBundle:Packages:add_package.html.twig', array('form' => $form->createView()));
    }

    public function packageInfoAPIAction($packageDownloadURL)
    {
        try {
            return new JsonResponse((new PackageInfo($packageDownloadURL))->toArray());
        } catch (Exception $e) {
            return new JsonResponse(array());
        }

    }

    public function downloadPackageAction($id)
    {
        $package = $this->getDoctrine()->getRepository('PSNDLMainBundle:Package')->find($id);

        if ($package === null) {
            $this->addFlash('danger', "Couldn't find the requested package.");
            return $this->redirectToRoute('psndl_main_packages');
        }

        $package->setDownloadCount($package->getDownloadCount() + 1);

        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($package);
        $em->flush();

        return $this->redirect($package->getDownloadURL());
    }

    public function downloadRapAction($id)
    {
        $package = $this->getDoctrine()->getRepository('PSNDLMainBundle:Package')->find($id);

        if ($package === null || $package->getRapName() === null || $package->getRapData() === null) {
            $this->addFlash('danger', "Couldn't find the requested rap.");
            return $this->redirectToRoute('psndl_main_packages');
        }

        $binary = hex2bin($package->getRapData());

        $response = new Response();

        $response->headers->add(array(
            'Content-Type' => 'application/octet-stream',
            'Content-Disposition' => ('attachment;filename=' . $package->getRapName()),
            'Content-Length' => strlen($binary),
            'Pragma:' => 'no-cache',
            'Expires' => '0'
        ));

        $response->setContent($binary);

        return $response;
    }

    public function downloadDatabaseAction(Request $request)
    {

        if ($request->isMethod('POST')) {
            $packages = $this->getDoctrine()->getRepository('PSNDLMainBundle:Package')->findBy(array('approved' => true));
            $dbAsText = '';

            foreach ($packages as $package) {
                $dbAsText .= $package->getPackageID() . ';';
                $dbAsText .= $package->getTitle() . ';';
                $dbAsText .= $package->getCategory()->getName() . ';';
                $dbAsText .= $package->getRegion()->getRegionCode() . ';';
                $dbAsText .= $package->getDownloadURL() . ';';
                $dbAsText .= $package->getRapName() . ';';
                $dbAsText .= $package->getRapData() . ';';
                $dbAsText .= str_replace("\r", '', str_replace("\n", '\n', $package->getDescription())) . ';';
                $dbAsText .= $package->getAuthor();
                $dbAsText .= "\n";
            }

            $response = new Response();

            $response->headers->add(array(
                'Content-Type' => 'text/plain',
                'Content-Disposition' => ('attachment;filename=db'),
                'Content-Length' => strlen($dbAsText),
                'Pragma:' => 'no-cache',
                'Expires' => '0'
            ));

            $response->setContent($dbAsText);

            return $response;
        }
        return $this->render('@PSNDLMain/Packages/download_old_db.html.twig');
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function acceptPackageAction($id)
    {
        $package = $this->getDoctrine()->getRepository('PSNDLMainBundle:Package')->find($id);

        if ($package === null) {
            $this->addFlash('danger', "Couldn't find the requested package");
            return $this->redirectToRoute('psndl_main_packages');
        }

        if ($package->getApproved() === true) {
            $this->addFlash('success', 'Package is already approved.');
        } else {
            $package->setApproved(true);

            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($package);
            $em->flush();

            $this->addFlash('success', 'Package has been approved.');
        }


        return $this->redirectToRoute('psndl_main_view_package', array('id' => $package->getId()));
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function editPackageAction($id, Request $request)
    {
        $package = $this->getDoctrine()->getRepository('PSNDLMainBundle:Package')->find($id);

        if ($package === null) {
            $this->addFlash('danger', "Couldn't find the requested package.");
            return $this->redirectToRoute('psndl_main_packages');
        }

        $form = $this->createForm(PackageEditType::class, $package, array('author' => $package->getAuthor()));

        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()) {
            $em = $this->getDoctrine()->getEntityManager();

            $em->persist($package);
            $em->flush();

            $this->addFlash('success', 'Successfully edited package.');

            return $this->redirectToRoute('psndl_main_view_package', array('id' => $id));
        }

        return $this->render('PSNDLMainBundle:Packages:edit_package.html.twig', array('form' => $form->createView()));
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function deletePackageAction($id, Request $request)
    {
        $package = $this->getDoctrine()->getRepository('PSNDLMainBundle:Package')->find($id);

        if ($package === null) {
            $this->addFlash('danger', "Couldn't find the requested package.");
            return $this->redirectToRoute('psndl_main_packages');
        }

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($package);
        $em->flush();

        $this->addFlash('success', 'Successfully deleted package.');

        return $this->redirectToRoute('psndl_main_packages');
    }

}
