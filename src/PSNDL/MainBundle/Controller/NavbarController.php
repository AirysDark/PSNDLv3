<?php

namespace PSNDL\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class NavbarController extends Controller
{
    public function sidebarAction() {
        $role = '';
        if($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $role = 'Administrator';
        } else if($this->get('security.authorization_checker')->isGranted('ROLE_USER')){
            $role = 'Normal User';
        } else if($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')){
            $role = 'No Rank';
        }
        return $this->render('@PSNDLMain/Navbar/sidebar.html.twig', array('role' => $role));
    }

    public function topbarAction() {
        return $this->render('@PSNDLMain/Navbar/topbar.html.twig');
    }

}
