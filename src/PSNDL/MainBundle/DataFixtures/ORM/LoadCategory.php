<?php


namespace PSNDL\MainBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use PSNDL\MainBundle\Entity\Category;

class LoadCategory implements FixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $categories = array(
            'Avatar',
            'C00',
            'Demo',
            'DLC',
            'EDAT',
            'Mini',
            'PS1',
            'PS2',
            'PSN',
            'PS4',
            'PSP',
            'PSVita',
            'Soundtrack',
            'Theme',
            'Update',
            'Video',
            'Wallpaper'
        );

        foreach($categories as $category) {
            $entity = new Category();
            $entity->setName($category);
            $manager->persist($entity);
        }
        $manager->flush();
    }
}