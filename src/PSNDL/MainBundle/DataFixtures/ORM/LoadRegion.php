<?php


namespace PSNDL\MainBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use PSNDL\MainBundle\Entity\Region;

class LoadRegion implements FixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $regions = array(
            'US' => 'United States',
            'EU' => 'Europe',
            'JP' => 'Japan',
            'HK' => 'Hong Kong',
            'ALL' => 'All Regions'
        );

        foreach($regions as $regionCode => $regionName) {
            $region = new Region();
            $region->setRegionCode($regionCode);
            $region->setRegionName($regionName);
            $manager->persist($region);
        }
        $manager->flush();
    }
}