<?php

namespace PSNDL\MainBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PackageType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('packageID', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Filled Automatically',
                    'readonly' => true
                ),
            ))
            ->add('title', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Title'
                )
            ))
            ->add('downloadURL', UrlType::class, array(
                'attr' => array(
                    'placeholder' => 'Download URL'
                )
            ))
            ->add('rapName', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Filled Automatically',
                    'readonly' => true
                ),
            ))
            ->add('rapData', TextType::class, array(
                'attr' => array(
                    'placeholder' => '16 bytes in hexadecimal'
                )
            ))
            ->add('description', TextareaType::class, array(
                'attr' => array(
                    'placeholder' => 'Description (optional)'
                )
            ))
            ->add('author', TextType::class, array(
                'attr' => array(
                    'placeholder' => 'Author'
                ),
                'data' => $options['author']
            ))
            ->add('category', EntityType::class, array(
                'class' => 'PSNDL\MainBundle\Entity\Category',
                'choice_label' => 'name',
                'placeholder' => 'Please select a category',
                'multiple' => false,
                'expanded' => false
            ))
            ->add('region', EntityType::class, array(
                'class' => 'PSNDL\MainBundle\Entity\Region',
                'choice_label' => 'regionName',
                'placeholder' => 'Please select a region',
                'multiple' => false,
                'expanded' => false
            ))
            ->add('save', SubmitType::class);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setRequired('author')
            ->addAllowedTypes('author', 'string');

        $resolver->setDefaults(array(
            'data_class' => 'PSNDL\MainBundle\Entity\Package',
            'author' => 'Unknown'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'psndl_mainbundle_package';
    }
}
